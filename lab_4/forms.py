from django import forms
import django.core.validators


class Message_Form(forms.Form):
    error_messages = {
        'required': 'Tolong masukan pesan anda',
        'invalid': 'Isi input dengan email',
    }
    attrs = {
        'class': 'form-control'
    }

    name = forms.CharField(label='Nama', required=False, max_length=27,
                           empty_value='Anonymous', widget=forms.TextInput(attrs=attrs))
    email = forms.EmailField(
        required=False, widget=forms.EmailInput(attrs=attrs))
    message = forms.CharField(
        widget=forms.Textarea(attrs=attrs), required=True, error_messages={'required': 'Tolong masukan pesan anda'})
